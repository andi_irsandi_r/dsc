import React, { Component } from 'react';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import './App.css';
import Halaman1 from './components/pages/Halaman1';
import Halaman2 from './components/pages/Halaman2';
import Home from './components/pages/Home';

export default class App extends Component{
  render(){
    return(
      <div>
        <Router>
          <Route path="/" exact component={Home} />
          <Route path="/halaman1" component={Halaman1} />
          <Route path="/page/2" component={Halaman2} />
        </Router>
      </div>
    )
  }
}
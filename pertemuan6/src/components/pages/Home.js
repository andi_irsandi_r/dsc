import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

export default class Home extends Component{

    state = {
        userData: [],
        age: '',
        name: '',
        email: '',
        id:'',
    }

    // componentDidMount(){
    //     axios.get('https://api.banghasan.com/quran/format/json/surat')
    //     .then( (response)=> {
    //         this.setState( {quran: response.data.hasil} )
    //     } )
    //     .catch( (error)=>{
    //         console.log(error)
    //     } )
    // }

    componentDidMount(){
        axios.get('http://api.bengkelrobot.net:8001/api/profile')
        .then( (response)=> {
            this.setState({ userData: response.data })
        } )
        .catch( (error)=>{
            console.log(error)
        } )
    }

    fillForm = (user) =>{
        this.setState({ id: user.id, name: user.name, age: user.age, email: user.email })
    }

    handleChange = (event) =>{
        this.setState({ [event.target.name]: event.target.value })
    }

    deleteUser = (id) =>{
        axios.delete('http://api.bengkelrobot.net:8001/api/profile/' + id).then(response=>{
            alert('Berhasil Hapus User')
            this.componentDidMount()
        })
    }

    submitData = (event) =>{
        event.preventDefault();
        const body = {
            name: this.state.name,
            email: this.state.email,
            age: parseInt(this.state.age),
        }
        if(this.state.id){
            axios.put('http://api.bengkelrobot.net:8001/api/profile/'+this.state.id, body)
            .then((response)=>{
                alert('Sukses Update Data');
                this.componentDidMount()
                this.setState({ age: '', email:'', name: '', id:'' })
            })
            .catch((error)=>{
                console.log(error)
            })
        }else{
            axios.post('http://api.bengkelrobot.net:8001/api/profile', body)
            .then((response)=>{
                alert('Sukses Tambah Data');
                let newUsers = this.state.userData;
                newUsers.unshift(response.data)
                this.setState({ userData: newUsers, age: '', email:'', name: '', id:'' })
            })
            .catch((error)=>{
                console.log(error)
            })
        }
       
    }

    

    render(){
        // console.log(this.state.userData)
        return(
            <div>
                <div className="tengah" style={{marginTop:20, display:'flex', justifyContent:'space-around'}}>
                    <h2>Home nih</h2>
                    <Link to="/halaman1">Ke halaman 1</Link>
                    <Link to="/page/2">Ke halaman 2</Link>
                    <a href="https://google.com">Ke google</a>
                </div>
                {/* <div style={{textAlign:'center'}}>
                    {this.state.quran.map( (q, index)=>{
                        return(
                            <div style={{ marginBottom: 10 }}>
                                <div key={index}> {q.nama} - ({q.name}) </div>
                                <div>{q.asma}</div>
                            </div>
                        )
                    } )}
                </div> */}
                <div style={{ margin: '0 50px' }}>
                    <form onSubmit={this.submitData} >
                        <input value={this.state.id} onChange={this.handleChange} type="text" placeholder="Id" name="id" />
                                <br/>
                                <br/>
                        <input value={this.state.name} onChange={this.handleChange} type="text" placeholder="Nama" name="name" required />
                                <br/>
                                <br/>
                        <input value={this.state.email} onChange={this.handleChange} type="email" placeholder="Email" name="email" required />
                                <br/>
                                <br/>
                        <input value={this.state.age} onChange={this.handleChange} type="number" placeholder="Umur" name="age" required />
                                <br/>
                                <br/>
                        <button type="submit">Simpan Data</button>
                    </form>
                </div>

                <div style={{ margin: '0 50px 100px' }}>
                    {this.state.userData.map( (user)=>{
                        return(
                            <div key={user.id} >
                                <h2>{user.name}</h2>
                                <div>Email: {user.email}</div>
                                <div>Umur: {user.age}</div>
                                <button onClick={()=> this.fillForm(user)}>Update User</button>
                                <button onClick={()=> this.deleteUser(user.id)}>Delete User</button>
                            </div>
                        )
                    } )}
                </div>
            </div>
        )
    } 
}
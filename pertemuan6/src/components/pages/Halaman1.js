import React from 'react';
import { Link } from 'react-router-dom';

export default function Halaman1(){
    return(
        <div className="tengah" style={{marginTop:20, display:'flex', justifyContent:'space-around'}}>
            <h2>Halaman 1 Nih</h2>
            <Link to="/">Kembali ke home</Link>
            <Link to="/page/2">Ke halaman 2</Link>
        </div>
    )
}
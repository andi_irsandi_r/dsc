import React, { Component } from 'react';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import './App.css';
import ForgotPassword from './components/pages/ForgotPassword';
import Halaman1 from './components/pages/Halaman1';
import Halaman2 from './components/pages/Halaman2';
import Home from './components/pages/Home';
import Login from './components/pages/Login';
import Register from './components/pages/Register';
import ChangeEmail from './components/pages/ChangeEmail'

export default class App extends Component{
  render(){
    return(
      <div>
        <Router>
          <Route path="/" exact component={Home} />
          <Route path="/halaman1" component={Halaman1} />
          <Route path="/page/2" component={Halaman2} />
          <Route path="/login" component={Login}/>
          <Route path="/register" component={Register}/>
          <Route path="/forgot-pass" component={ForgotPassword}/>
          <Route path="/change-email" component={ChangeEmail}/>
        </Router>
      </div>
    )
  }
}
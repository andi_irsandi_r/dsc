import firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyBlN87d-jh7thEE-0UlgJTrTBOKERWq5mg",
    authDomain: "fir-functiondsc.firebaseapp.com",
    projectId: "fir-functiondsc",
    storageBucket: "fir-functiondsc.appspot.com",
    messagingSenderId: "1019486023267",
    appId: "1:1019486023267:web:17f05d7c6f788bab3cd256",
    measurementId: "G-HSQ4NCFB1F"
};

let appConfig = firebase.initializeApp(firebaseConfig);
export let fDB = appConfig.database()
export let fAuth = appConfig.auth()
export let fStore = appConfig.firestore()

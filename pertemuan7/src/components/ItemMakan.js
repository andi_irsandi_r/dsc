import React from 'react'

export function ItemMakan(props){
    return(
      <div className="box-makan">
        <div>{props.item?.nama}</div>
        <div>Rp. {props.item?.harga}</div>
      </div>
    )
}
import React from 'react';
import { Link } from 'react-router-dom';

export default function Halaman2(){
    return(
        <div className="tengah" style={{marginTop:20, display:'flex', justifyContent:'space-around'}}>
            <h2>Halaman 2 Nih</h2>
            <Link to="/halaman1">Ke halaman 1</Link>
            <Link to="/">Kembali ke Home</Link>
        </div>
    )
}
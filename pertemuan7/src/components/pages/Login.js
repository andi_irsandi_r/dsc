import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Paper, TextField } from '@material-ui/core';
import { fAuth } from '../../config/firebase';

export default class Login extends Component{

    state ={
        email:'',
        password:''
    }

    handleChange = (event) =>{
        this.setState({ [event.target.name]: event.target.value })
    }

    handleLogin = () =>{
        const {email, password} = this.state;
        fAuth.signInWithEmailAndPassword(email, password)
        .then((userCredential) => {
            console.log(userCredential)
            // Signed in
            // let user = userCredential.user;
            // ...
            if(userCredential.user.emailVerified){
                this.props.history.push('/')
            }else{
                alert('Verifikasi Email Anda terlebih dahulu!!!')
            }
        })
        .catch((error) => {
            // var errorCode = error.code;
            // var errorMessage = error.message;
            alert(error.message)
        });
    }

    render(){
        return(
            <div style={{margin:20}}>
                <h1>Ini Halaman Login</h1>
                <Paper style={{width:'50vh', padding:15}}>
                    <TextField value={this.state.email} onChange={this.handleChange} name="email" style={{marginBottom:20}} label="Email" type="Email" size="small" variant="outlined" fullWidth />
                    <TextField value={this.state.password} onChange={this.handleChange} name="password" style={{marginBottom:20}} label="Password" type="Password" size="small" variant="outlined" fullWidth />

                    <Button variant="contained" size="small" color="primary" onClick={this.handleLogin}>
                        Login
                    </Button>

                    <div style={{marginTop:10}}>
                        Belum punya akun? Register <Link to="/register">di sini</Link>
                    </div>
                    <div style={{marginTop:10}}>
                        Lupa Password? Klik <Link to="/forgot-pass">di sini</Link>
                    </div>
                </Paper>
            </div>
        )
    }
}
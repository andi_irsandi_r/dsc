import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Paper, TextField } from '@material-ui/core';
import { fAuth } from '../../config/firebase';

export default class Register extends Component{

    state ={
        email:'',
        password:''
    }

    handleChange = (event) =>{
        this.setState({ [event.target.name]: event.target.value })
    }

    registerUser = () =>{
        const {email, password} = this.state
        if(email && password){
            fAuth.createUserWithEmailAndPassword(email, password)
            .then(()=>{
                        fAuth.currentUser.sendEmailVerification()
                        .then(()=>{
                            alert('Check Email Anda untuk memverifkasi akun')
                            this.setState({email:'', password:''})
                        })
                        .catch((error)=>{
                            alert('gagal registrasi dengan alasan: '+ error.message)
                        })
            })
            .catch((error)=>{
                alert('gagal registrasi dengan alasan: '+ error.message)
            })
        }else{
            alert('Field Belum Lengkap')
        }
    }
    
    render(){
        return(
            <div style={{margin:20}}>
                <h1>Ini Halaman Register</h1>
                <Paper style={{width:'50vh', padding:15}}>
                    <TextField value={this.state.email} onChange={this.handleChange} name="email" style={{marginBottom:20}} label="Email" type="Email" size="small" variant="outlined" fullWidth />
                    <TextField value={this.state.password} onChange={this.handleChange} name="password" style={{marginBottom:20}} label="Password" type="Password" size="small" variant="outlined" fullWidth />

                    <Button variant="contained" size="small" color="primary" onClick={this.registerUser}>
                        Register
                    </Button>

                    <div style={{marginTop:10}}>
                        Sudah punya akun? Login <Link to="/login">di sini</Link>
                    </div>
                </Paper>
            </div>
        )
    }
}
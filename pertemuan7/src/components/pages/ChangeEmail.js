import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Paper, TextField } from '@material-ui/core';
import { fAuth } from '../../config/firebase';

export default class ChangeEmail extends Component{

    state ={
        emailLama:'',
        emailBaru:'',
        password:''
    }

    handleChange = (event) =>{
        this.setState({ [event.target.name]: event.target.value })
    }

    handleChangeEMail = () =>{
        const {emailLama, emailBaru, password} = this.state;
       fAuth.signInWithEmailAndPassword(emailLama, password)
       .then(()=>{
            fAuth.currentUser.updateEmail(emailBaru).then(()=>{
            // fAuth.currentUser.updatePassword(passwordBaru).then(()=>{
                fAuth.currentUser.sendEmailVerification()
                        .then(()=>{
                            alert('Check Email Anda untuk memverifkasi akun')
                            fAuth.signOut().then(()=>{
                                this.props.history.push('/login')
                            }).catch(error=>{
                                alert(error.message)
                            })
                        })
                        .catch((error)=>{
                            alert('gagal registrasi dengan alasan: '+ error.message)
                        })
            }).catch((error)=>{
                alert(error.message)
            })
       })
       .catch((error)=>{
            alert(error.message)
       })
    }

    render(){
        return(
            <div style={{margin:20}}>
                <h1>Ini Halaman Ubah Email</h1>
                <Paper style={{width:'50vh', padding:15}}>
                    <TextField value={this.state.emailLama} onChange={this.handleChange} name="emailLama" style={{marginBottom:20}} label="Email Lama" type="Email" size="small" variant="outlined" fullWidth />
                    <TextField value={this.state.emailBaru} onChange={this.handleChange} name="emailBaru" style={{marginBottom:20}} label="Email Baru" type="Email" size="small" variant="outlined" fullWidth />
                    <TextField value={this.state.password} onChange={this.handleChange} name="password" style={{marginBottom:20}} label="Password" type="Password" size="small" variant="outlined" fullWidth />

                    <Button variant="contained" size="small" color="primary" onClick={this.handleChangeEMail}>
                        Update Email
                    </Button>

                    <div style={{marginTop:10}}>
                        Belum punya akun? Register <Link to="/register">di sini</Link>
                    </div>
                </Paper>
            </div>
        )
    }
}
const functions = require("firebase-functions");
const express = require("express");
// const cors = require("cors");

const admin = require("firebase-admin");
admin.initializeApp()

const app = express();
const postingan = 'postingan';

app.get("/", async (req, res)=>{
  const data = await admin.firestore().collection(postingan).get()

  const allData = []

  data.forEach((snapshot) =>{
    let id = snapshot.id;
    let content = snapshot.data()
    allData.push({id, content})
  })

  res.status(200).send(JSON.stringify(allData))
});

app.get("/:id", async(req,res)=>{
  const data = await admin.firestore().collection(postingan).doc(req.params.id).get()

  let id = data.id
  let content = data.data()

  res.status(200).send(JSON.stringify({id, content}))
})

app.post("/", async (req, res)=>{
    await admin.firestore().collection(postingan).add(req.body)
    .then(()=>{
        res.status(200).send('Sukses Menambahkan Status')
    })
    .catch(()=>{
        res.status(500).send('Gagal Menambahkan Status')
    })
})

app.patch("/:id", async (req, res)=>{
  const body = req.body
  await admin.firestore().collection(postingan).doc(req.params.id).update(body)
  res.status(200).send('Sukses Mengupdate Status')
});

app.delete("/:id", async (req, res)=>{
  await admin.firestore().collection(postingan).doc(req.params.id).delete()
  res.status(200).send('Sukses Menghapus Status')
});

exports.postingan = functions.https.onRequest(app);

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onReq((req, res) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   res.send("Hello from Firebase!");
// });

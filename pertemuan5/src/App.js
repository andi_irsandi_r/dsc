import React, { Component } from 'react';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import './App.css';
import Halaman1 from './components/pages/Halaman1';
import Halaman2 from './components/pages/Halaman2';
import Home from './components/pages/Home';
// import { ItemMakan } from './components/ItemMakan';

// export default class MyComponent extends Component{
//   state = {
//     foods: [ 
//       {nama: 'Jagung Bakar', harga: '20.000'},
//       {nama: 'Nasi Goreng', harga: '10.000'},
//       {nama: 'Bakwan', harga: '5.000'},
//     ],
//     drinks: [ 
//       {nama: 'Jus Alpukat', harga: '20.000'},
//       {nama: 'Kopi Susu', harga: '10.000'},
//       {nama: 'Taro', harga: '5.000'},
//     ]
//   }

//   render(){
//     return(
//       <div>
//         <h1 className="tengah">Irsandi Caffee</h1>
//         <div style={ { display:'flex', flexDirection:'row', justifyContent:'space-around' } }>
//           <div>
//             <h3>Makanan</h3>
//             {this.state.foods.map((food, index)=>(
//               <ItemMakan item={food} key={index} />
//             ))}

//           </div>
//           <div>
//             <h3>Minuman</h3>
//             {this.state.drinks.map((drink, index)=>(
//               <ItemMakan item={drink} key={index} />
//             ))}
//           </div>
//         </div>
//       </div>
//     )
//   }
// }

export default class App extends Component{
  render(){
    return(
      <div>
        <Router>
          <Route path="/" exact component={Home} />
          <Route path="/halaman1" component={Halaman1} />
          <Route path="/page/2" component={Halaman2} />
        </Router>
      </div>
    )
  }
}
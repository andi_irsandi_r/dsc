import React from 'react';
import { Link } from 'react-router-dom';

export default function Home(){
    return(
        <div className="tengah" style={{marginTop:20, display:'flex', justifyContent:'space-around'}}>
            <h2>Home nih</h2>
            <Link to="/halaman1">Ke halaman 1</Link>
            <Link to="/page/2">Ke halaman 2</Link>
            <a href="https://google.com">Ke google</a>
        </div>
    )
}